### classes[2] = "Clean coding"

#### Laboratório de Bioinformática 2024-2025

![Logo EST](assets/logo-ESTB.png)

Francisco Pina Martins

<p><img style="vertical-align:middle" src="../presentation_assets/mastodon-icon.svg" alt="Mastodon icon" width="40px", height="40px"> <a href="https://scholar.social/@FPinaMartins">@FPinaMartins@scholar.social</a></p>

---

### What is "clean coding"?

* &shy;<!-- .element: class="fragment" -->No clear definition, but...
    * &shy;<!-- .element: class="fragment" -->Easy to read and change
    * &shy;<!-- .element: class="fragment" -->Expressive!
    * &shy;<!-- .element: class="fragment" -->Less is more
    * &shy;<!-- .element: class="fragment" -->Tested

<div class="fragment">

![Scumbag dev](assets/scumbag.jpg)

</div>

---

### Easy to read and change

* &shy;<!-- .element: class="fragment" -->Focused
    * &shy;<!-- .element: class="fragment" -->Each unit does only one thing
    * &shy;<!-- .element: class="fragment" -->No redundancy
* &shy;<!-- .element: class="fragment" -->Logic is simple to follow
    * &shy;<!-- .element: class="fragment" -->[KISS](http://en.wikipedia.org/wiki/KISS_principle) (Keep it simple, stupid)
    * &shy;<!-- .element: class="fragment" -->Avoid large blocks of convoluted logic

<div class="fragment">

![KISS (Rock band)](assets/kiss.jpg)

</div>

|||

### Easy to read and change

<div class="fragment" style="float: left; width:50%">

Instead of:

``` python
infile = open('file.txt')
names = []
for lines in infile:
    names.append(lines.split[0])
infile.close()

infile = open("file.txt")
data = []
for lines in infile:
    data.append(lines.split[1:])
infile.close()
```

</div>
<div class="fragment" style="float: right; width:50%">

...Do...

``` python
with open("file.txt", "r") as infile:
    names = []
    data = []
    for lines in infile:
        names.append(lines[0])
        data.append(lines[1:])
```

</div>

---

### Expressive

* &shy;<!-- .element: class="fragment" -->Meaningfull variable names
    * &shy;<!-- .element: class="fragment" -->`target_taxa` >>> `tt` 
    * &shy;<!-- .element: class="fragment" -->`for species in genus` >>> `for x in gen`
* &shy;<!-- .element: class="fragment" -->Use <span style="color:blue">nouns</span> for <span style="color:blue">variables</span> and <span style="color:yellow">verbs</span> for <span style="color:yellow">functions</span>
    * &shy;<!-- .element: class="fragment" -->`def tree_plotter()` >>> `def trees()`
    * &shy;<!-- .element: class="fragment" -->`spp_list = []` >>> `spp_gatherer = []`

<div class="fragment">

![Expressive code](assets/expressive.jpg)

</div>

|||

### Expressive

<div class="fragment" style="float: left; width:50%">

Instead of:

``` python
f = open("file.txt", "r")
n = []
d = []
for x in f:
    n.append(x[0])
    d.append(x[1:])
f.close()
```

</div>
<div class="fragment" style="float: right; width:50%">

...Do...

``` python

with open("file.txt", "r") as infile:
    names = []
    data = []
    for lines in infile:
        names.append(lines[0])
        data.append(lines[1:])
```

---

### Less is more!

* &shy;<!-- .element: class="fragment" -->Avoid unnecessary dependencies
    * &shy;<!-- .element: class="fragment" -->Maintenance burden
    * &shy;<!-- .element: class="fragment" -->Harder installation
    * &shy;<!-- .element: class="fragment" -->Do you *really* need `biopython` to access Entrez?
* &shy;<!-- .element: class="fragment" -->Use list comprehension instead of `for` loop
* &shy;<!-- .element: class="fragment" -->Avoid redundancies

<div class="fragment">

![Less is more](assets/less_is_more.jpg)

</div>

|||

### Less is more

<div class="fragment" style="float: left; width:50%">

Instead of:

``` python
if something:
   boilerplate
   boilerplate
   boilerplate
   boilerplate
   specific_to_something
elif something_else:
   boilerplate
   boilerplate
   boilerplate
   boilerplate
   specific_to_somehitng_else
elif something_different:
   boilerplate
   boilerplate
   boilerplate
   boilerplate
   specific_to_somehitng_different
else:
   boilerplate
   boilerplate
   boilerplate
   boilerplate
   specific_to_else
```
</div>
<div class="fragment" style="float: right; width:50%">

...Do...

``` python
def boilerplate():
    boilerplate
    boilerplate
    boilerplate
    boilerplate
    return important_stuff

if something:
   boilerplate()
   specific_to_something
elif something_else:
   boilerplate()
   specific_to_somehitng_else
elif something_different:
   boilerplate()
   specific_to_somehitng_different
else:
   boilerplate()
   specific_to_else
```

</div>

---

### Testing

* &shy;<!-- .element: class="fragment" -->(Ideally) All of your code should be tested
    * &shy;<!-- .element: class="fragment" -->Each task is a unit
    * &shy;<!-- .element: class="fragment" -->Each unit does only one thing
* &shy;<!-- .element: class="fragment" -->Use a framework like `pytest` or `nose`
* &shy;<!-- .element: class="fragment" -->Use a self contained environment

<div class="fragment">

![Lack of tests](assets/LackofTests.jpg)

</div>

|||

### Testing

* &shy;<!-- .element: class="fragment" -->Testing works by providing a unit with:
    * &shy;<!-- .element: class="fragment" -->Known input
    * &shy;<!-- .element: class="fragment" -->Expected output
* &shy;<!-- .element: class="fragment" -->And comparing results

|||

### Testing

```python
# my_function.py
def square_number(number):
    """
    Squares a number.
    """
    square = number * number
    return square
```

|||

### Testing

```python
# test_my_function.py
import pytest
import my_function


def test_square_number():
    """
    Tests if square_number() really is squaring a number
    """
    known_inputs = [2, 4, 6]
    expected_outputs = [4, 16, 36]

    for i, j in zip(known_inputs, expected_outputs):
        assert my_function.square_number(i) == j
```

---

### Code styling

* &shy;<!-- .element: class="fragment" -->Coding conventions are important
* &shy;<!-- .element: class="fragment" -->Ensures every project member writes code in the same way
    * &shy;<!-- .element: class="fragment" -->Identation
    * &shy;<!-- .element: class="fragment" -->Line length
    * &shy;<!-- .element: class="fragment" -->Operator splitting
    * &shy;<!-- .element: class="fragment" -->Blank lines
    * &shy;<!-- .element: class="fragment" -->Many more!
* &shy;<!-- .element: class="fragment" -->Use packages to control for this
    * &shy;<!-- .element: class="fragment" -->PEP8 (pycodestyle)
    * &shy;<!-- .element: class="fragment" -->pylint

|||

### Code styling

```bash
pycodestyle my_function.py
pylint my_function.py
```

---

### References

* [Python clean code](https://github.com/zedr/clean-code-python)
* [pyTest](https://pytest.org/en/latest/)
* [PEP8](https://www.python.org/dev/peps/pep-0008/)

